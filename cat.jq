#/usr/local/bin/jq -rnf

def glyphs: " <({0%VSO8R@$$"/"";
def glyph($max): (glyphs)[./$max * (glyphs|length-1)|floor] // ".";

def invglyph($max): $max - . | glyph($max);
def oldcircplot($widthmul)
  : . as$width
  | ($width / 2 | floor) as $halfwidth
  | {x: $halfwidth, y: $halfwidth} as $center
  | ({x: 0, y: 0} | dist($center) + 0.5) as $max_distance
  | [ range($width)
    | . as $y
    | [ {x: (range($width*$widthmul)/$widthmul), y: $y}
      | dist($center) | invglyph($max_distance) ]
    ]
  | map(add) | join("\n")
;

def depthplot($width; $height; $stretch; fn)
  : [ range($height) as $y
    | [ { x: (range($width * $stretch)/$stretch)
        , y: $y
        , w: $width
        , h: $height
        }
      | fn | glyph(1)
      ]
    | add
    ]
  | join("\n")
;
def depthplot($w; $h; f): depthplot($w; $h; 1; f);
def depthplot(f): depthplot(40;40;2;f);

def square: .*.;
def dist($o): (.x-$o.x|square)+(.y-$o.y|square)|sqrt;

def center: {x: (.w/2), y: (.h/2)};
def circ: center as $center | dist($center)/({x: 0, y: 0}|dist($center));
def distances($points; combine):
  ([dist($points[])]|combine)
   / ({x: .w, y: .h} | dist({x: 0, y: 0}))
;

def product: reduce .[] as $n (1; .*$n);
def aritmean: add / length;
def geommean: pow(product; 1/length);

def abs: if .<0 then -. else . end;
def numsclose($a; $b; $t): $a - $b | abs < $t;
def close($o; $tolerance): numsclose(.x; $o.x; $tolerance) and numsclose(.y; $o.y; $tolerance);
def slope($a; $b): try ($b.y - $a.y) / ($b.x - $a.x);
def aboutsame($a; $b; $t): $a - $b | abs < $t;
def on_line($it; $line; $tolerance):
  numsclose( $line[1].y
           ; $it.y + slope($line[0]; $it)*($line[1].x - $it.x)
           ; $tolerance
           )
;
def num_between($a; $b; $c):
  ($a <= $b and $b <= $a)
or($a >= $b and $b >= $a)
;
def within_rect($it; $rect):
      num_between($rect[0].x; $it.x; $rect[1].x)
  and num_between($rect[0].y; $it.y; $rect[1].y)
;
def between($it; $line; $tolerance):
  within_rect($it; $line)# and on_line($it; $line; $tolerance)
;
def b2n: if . then 1 else 0 end;
def inwire($lines; $tolerance):
  . as $point
  | $lines | any(between($point; .; $tolerance))
  | b2n
;
def wireframe($w; $h; $stretch; $lines; $tolerance):
  depthplot($w; $h; $stretch; inwire($lines; $tolerance))
;

def pairs($acc):  # [3,2,5] => [[3,2],[3,5],[2,5]]
  if .==[] then $acc
  else.
  | .[0] as $head
  | .[1:]
  | pairs($acc + map([$head,.]))
  end
;
def pairs: pairs([]);

[ {x: 20, y: 35}  # left leg
, {x: 25, y: 25}  # left bottom body
, {x: 60, y: 25}  # right bottom body
, {x: 65, y: 35}  # right leg
, {x: 66, y: 14}  # right end
, {x: 73, y: 10}  # tail
, {x: 46, y: 16}  # right back
, {x: 24, y: 16}  # left back
, {x: 40, y: 26}  # belly
, {x: 20, y: 06}  # top head
, {x: 15, y: 10}  # top head
] as $cat_points |
   
def cat: $cat_points | depthplot(80; 40; 2; distances($cat_points; geommean));

($cat_points|pairs) as $cat_lines |

def wireframe_cat: wireframe(80; 40; 2; $cat_lines; .);

cat


