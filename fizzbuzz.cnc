
(define [fzbz n]
  : = (mod n 15) 0 "fizzbuzz"
  : = (mod n 03) 0 "fizz"
  : = (mod n 05) 0 "buzz"
   : ->str n)

(define [main]
  $ loop \next {n IO}
    [< n 1
      \IO
    :: next (- n 1) $put-str-ln (fzbz n) IO]
    100)