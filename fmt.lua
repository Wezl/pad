local F = {
  numberperiodseparator = false,
  elementseparator = ", " or "; ",
  multilineelementseparator = "\n, " or ",\n  " or "\n, " or ",\n  "
}

local print, type, tostring, getmetatable = print, type, tostring, getmetatable

function F.print(a, ...)
  if #{...} == 0 then print(F.any(a))
  else
    print(F.any(a))
    return F.print(...)
  end
end
function F.printline(v, nest, inline)
  if inline == nil then inline = true end
  print(F.any(v, nest or 2, inline))
end
function F.any(v, nest, inline)
  nest = nest or 3
  local t = type(v)
  if t == "table" then
    return F.table(v, nest, inline)
  elseif t == "string" then
    return F.string(v, nest, inline)
  elseif t == "number" and F.numberperiodseparator then
    return F.number(v, F.numberperiodseparator)
  elseif t == "boolean" or t == "nil" or t == "number" then
    return tostring(v)
  elseif nest==0 then
    return "#<"..t..">"
  else
    return "#<"..tostring(v)..">"
  end
end
F.__tostring = F.any
F.tostring = F.any

function F.string(s, nest, inline)
  if inline or nest == 0 then
    return ("%q"):format(s
      :sub(1, 70) .. (#s > 70 and "..." or "")
     ):gsub("\\\n", "\\n")
  else
    return ("%q"):format(s)
  end
end

function F.table(t, nest, inline)
  nest = nest or 3
  if nest == 0 then return "{#="..#t.."}" end
  local m = getmetatable(t)
  if m and m.__tostring and m.__tostring ~= F.__tostring then
    return tostring(t)
  end
  local a = "{"
  local first = true
  local sep = inline and F.elementseparator or F.multilineelementseparator
  local k
  for k in pairs(t) do
    a = a .. (first and " " or sep)
          .. "["..F.any(k, 1, true).."] = "
          .. F.any(t[k], nest - 1, true)
    first = false
  end
  return inline and a .. " }" or a .. "\n}"
end

function F.number(n, sep)
  sep = sep or ","
  return tostring(n)
         :reverse()
         :gsub("(%.?)(.*)",
           function(decimalpoint, integer)
             return decimalpoint
                 .. integer
                    :gsub("(%d%d%d)()",
                      function(period, ind)
                        if integer:sub(ind, ind):match("[0-9]") then
                        -- emulating positive lookahead like
                        -- /(\d{3})(?=\d)/
                          return period..sep
                        else
                          return period
                        end
                      end)
           end)
         :reverse()
end

return F

