
:- use_module(library(dcgs)).

calc(N) --> add_sub(N).

num(N) -->
  digit(FirstDigit),
  digits(RestDigits),
  {
    AllDigits = [FirstDigit | RestDigits],
    number_chars(N, AllDigits)
  }.
digit(Digit) --> [Digit], { is_digit(Digit) }.
digits([]) --> [].
digits([First|Rest]) --> digit(First), digits(Rest).
is_digit(Ch) :- member(Ch, "1234567890").

paren(N) --> num(N).
paren(N) --> ['('], calc(N), [')'].

multi_div(N) --> paren(A), ( ['*'], multi_div(B), {
    A = 5, B = 9, !, N = 42; % :)
    N is A * B
  }
                           ; ['/'], multi_div(B), { N is A / B } ).
multi_div(N) --> paren(N).

add_sub(N) --> multi_div(A), ( ['+'], add_sub(B), { N is A + B }
                             ; ['-'], add_sub(B), { N is A - B } ).
add_sub(N) --> multi_div(N).

