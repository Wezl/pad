
(require "lib")
(require "short")
(require "stringmap")
(require "loop")

#!# (readjson stream) => (: result stream-left)
 ! read a json object from a stream (list of characters)
 ! JSON     | ConCurr
 ! =========|========
 ! Object   | stringmap
 ! Array    | list
 ! Boolean  | function (true) or (false)
 ! Null     | {}
 ! String   | string
 ! Number   | number
 !#
(define [readjson s]
  : let \s (skipws s)
   : {"{"  _} s [readobject s]
   : {"["  _} s [readarr    s]
   : {"\"" _} s [readstring s]
   # eww, keywords are so awful
   : {"n" "u" "l" "l" s'} s [cons {} s']
   : {"t" "r" "u" "e" s'} s [cons (true) s']
   : {"f" "a" "l" "s" "e" s'} s [cons (false) s']
    : readnumber s) # most general case


(define [skipws s]
   : {} s [unexpected\EOF]
   : {c s'} s
      [if (any (= c) {" " "\t" "\n"})
         [skipws s']
         : s]
    : typeerror s)

(define [readarray s]
  : let \s (skipws s)
   : {"[" s'} s
      [loop \next{s arr}
        [let \s (skipws s)
         : {"]" s'} s [cons (reverse arr) s']
         # pretty ugly, just ignores ,s because
         # JSON should be like lisp and not need
         # separators
         : {"," s'} s [:next s' arr]
         : {item s'} (readjson s)
            [:next s' $cons item arr]
          : fail() ]
        s' {}]
   : {* s} s
      [unexpected *]
    : typeerror s)

(define [readobject s]
  : let \s (skipws s)
   : {"{" s'} s
      [loop \next{s obj}
        [let \s (skipws s)
         : {"}" s'} s [cons (reverse arr) s']
         : {"," s'} s [:next s' obj]
         : {key s'} (readstring s)
           [let \s' (skipws s')
            : {":" s''} s'
              [let \s'' (skipws s'')
               : {val s'''} (readjson s'')
                  [:next s''' (addpair key val obj)]
                : fail() ]
             : unexpected (l s) ]
          : fail() ]
        s' (stringmap {})]
   : {* s} s
      [unexpected *]
    : typeerror s)

(define [readstring s]
   : {"\"" s'} s
      [loop \next {s str}
         [ {"\"" s'} s
            [cons str s']
         : {"\\" "u" a b c d s'} s
            [notimplemented()]
         : {"\\" c s'} s
            [:next s' $strcat str $case-with (= c)
              {"\\" "\\"
               "n" "\n"
               "t" "\t"
               "/" "/"
               "\"" "\""
               "b" [ascii 8]
               "f" [ascii 12]
               "r" [ascii 13]
               (unknown_escape c) } ]
         : {c s'} s
            [:next s' $strcat str c]
          : unterminatedstring()
         ]
         s' "" ]
   : {} s [unexpected \EOF]
   : {* _} s [unexpected *]
    : typeerror() )

