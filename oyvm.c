#include <stdio.h>
#include <string.h>
#include "oyvm.h"

int main(int argc, char **argv){
  int ret = 0;
  int raw = 0;
  Hp heap;
  if(argc < 2){
    fputs(
      OYVM_VERSION
      "usage: oyvm [--start|-s <addr>] [--zero|-z] [--help|-?]\n"
      "            [--asm|-a] <preload> [--run|-r] [--raw|-R]",
      stderr
    );
    return 1;
  } else {
    Nm start = 0;
    for(int argn = 1; argn < argc; argn++){
      if(!strcmp(argv[argn], "--zero")
      || !strcmp(argv[argn], "-z")){
        for(int i=0; i<Sz; i++)
          heap.mem[i] = (Nm) 0;
        heap.top = (Nm) 0;
      } else if(!strcmp(argv[argn], "--help")
           || !strcmp(argv[argn], "-?")){
        puts(
          OYVM_VERSION
          "========|===================================================\n"
          "--zero  | -z Zero out the playing field, recommended to be\n"
          "        '    the first option\n"
          "--start | -s Takes an address for the instruction pointer\n"
          "        |    to start at, defaulting to 0 which is probably\n"
          "        '    what you want\n"
          "--help  ' -? Shows this message\n"
          "--asm   | -a Use the assembler for optimal Legibility(tm).\n"
          "        '    Example: '$5>$0->=$U?' -> '$5 EQL #lbl JNZ'\n"
          "--run   | -r Run the input. This can be specified multiple\n"
          "        '    times for some reason.\n"
          "--raw   | -R Output final state as raw bytes. This is\n"
          "        |    helpful to tell the difference between the\n"
          "        |    character '5' and the number 0x05. Try piping\n"
          "        '    through hd."
        );
        return 0;
      } else if(!strcmp(argv[argn], "--asm")
             || !strcmp(argv[argn], "-a")){
        fputs("NYI", stderr); return 1;
      } else if(!strcmp(argv[argn], "--start")
             || !strcmp(argv[argn], "-s")){
        unsigned int n;
        if( argn == argc-1 ){
          fputs("--start needs an option", stderr);
          return 1;
        }
        if( sscanf(argv[++argn], "%u", &n) != 1 ){
          fputs("start needs an unsigned number", stderr);
          return 1;
        };
        start = (Nm) n;
      } else if(!strcmp(argv[argn], "--run")
             || !strcmp(argv[argn], "-r")){
        ret = oyvm_run(&heap, start);
      } else if(!strcmp(argv[argn], "--raw")
             || !strcmp(argv[argn], "-R")){
        raw = 1;
      } else {
        for(char *preload=argv[argn]; *preload && heap.top<Sz; heap.top++){
          heap.mem[heap.top] = *(preload++);
        }
      }
    }
  }
  if(raw){
    for(int i=0; i<Sz; i++) putchar(heap.mem[i]);
    putchar(heap.top);
  } else PrHp(heap);
  return ret;
}

char Nm2Ch(Nm n){
  if(n <  10 ) return n + '0';
  if(n <  ' ') return ' ';
  if(n >= '~') return '#';
  return n;
}

void PrHp(Hp heap){
  for(int i=0; i<Sz; i+=64){
    for(int j=0; j<64; j++)
      putchar( i+j == heap.top ? '~'
               : Nm2Ch(heap.mem[i+j]) );
    putchar('\n');
  }
}

#define MAXSTEPS 1024
Nm oyvm_run(Hp *heap, Nm ip){
  for(int step=0; step<MAXSTEPS; step++){
    switch(heap->mem[ip++]){
      case I_NOP:
        break;
      case I_LIT:
        heap->mem[heap->top] = heap->mem[ip++];
        break;
      case I_END:
        return heap->mem[heap->top];
        break;
      case I_PSH:
        heap->top++;
        break;
      case I_POP:
        heap->top--;
        break;
      case I_GET:
        heap->mem[heap->top] = heap->mem[heap->mem[heap->top]];
        break;
      case I_PUT:
        heap->mem[heap->mem[heap->top]] = heap->mem[heap->top-1];
        break;
#   define op(instr, o) case instr: \
        heap->top--; \
        heap->mem[heap->top] o heap->mem[heap->top + 1]; \
        break;
      op(I_ADD, +=)
      op(I_MUL, *=)
      op(I_DIV, /=)
      op(I_SUB, -=)
#   undef op
      case I_EQL:
        heap->mem[heap->top] = !( heap->mem[heap->top]
                                - heap->mem[heap->top - 1] );
        break;
      case I_NOT:
        heap->mem[heap->top] = ! heap->mem[heap->top];
        break;
      case I_INC:
        heap->mem[heap->top]++;
        break;
      case I_DEC:
        heap->mem[heap->top]--;
        break;
      case I_CPY:
        heap->mem[heap->top] = heap->mem[heap->top-1];
        break;
      case I_JNZ:
        if(!heap->mem[heap->top-1]){
          heap->top -= 2;
          break;
        }
        ip = heap->mem[heap->top--];
        break;
      case I_JMP:
        ip = heap->mem[heap->top];
        break;
      case I_SKP:
        ip++;
        break;
      case I_EXE:
        ip = heap->top;
        break;
      case I_INP:
        heap->mem[heap->top] = getchar();
        if(heap->mem[heap->top] == EOF) heap->mem[heap->top] = 0;
        break;
      case I_OUT:
        putchar(heap->mem[heap->top]);
        break;
      case I_ERR:
        fputs("error!", stderr);
        return 1;
        break;
      default:
        fprintf(stderr,
               "unknown instruction code: %u '%c'\n",
               heap->mem[ip-1],
               Nm2Ch(heap->mem[ip-1]));
        return 1;
    }
  }
  fputs("Max cycles reached", stderr);
  return 1;
}
#undef MAXSTEPS

