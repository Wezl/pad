#ifndef OYVM_H
#define OYVM_H

#define OYVM_VERSION "Oh, Why!? VM version -0.0\n"

typedef unsigned char Nm;
#define Sz 256
typedef char Ch;
typedef char*St;

typedef struct Heap_struct {
  Nm mem[Sz];
  Nm top;
} Hp;

char Nm2Ch(Nm n);
void PrHp(Hp heap);
Nm oyvm_run(Hp *heap, Nm start);

enum {
  I_ADD = '+',
  I_DIV = '/',
  I_DEC = '(',
  I_CPY = ':',
  I_END = '@',
  I_EQL = '=',
  I_ERR = 0,
  I_EXE = ';',
  I_GET = '[',
  I_INP = '{',
  I_INC = ')',
  I_JMP = '!',
  I_JNZ = '?',
  I_LIT = '$',
  I_MUL = '*',
  I_NOP = ' ',
  I_NOT = '~',
  I_OUT = '}',
  I_POP = '<',
  I_PSH = '>',
  I_PUT = ']',
  I_SKP = '#',
  I_SUB = '-'
};

#endif

